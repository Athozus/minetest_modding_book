---
title: Front Cover
layout: default
homepage: true
no_header: true
root: ..
idx: 0.1
---

<header>
    <h1>Minetest Modding Book</h1>

    <span>par <a href="https://rubenwardy.com" rel="author">rubenwardy</a></span>
    <span>édité par <a href="http://rc.minetest.tv/">Shara</a></span>
</header>

## Introduction

Minetest utilise des scripts Lua pour fournir un support de modding.
Ce livre a pour but de vous apprendre à créer vos propres mods, en partant
des bases.
Chaque chapitre se concentre sur une partie particulière de l'API et vous
permettra bientôt de créer vos propres mods. 

En plus de [lire ce manuel en ligne](https://rubenwardy.com/minetest_modding_book),
vous pouvez également le [télécharger en HTML](https://gitlab.com/rubenwardy/minetest_modding_book/-/releases).

### Avis et Contributions

Vous avez remarqué une erreur, ou vous souhaitez donner votre avis ?
Parlez-moi en.

* Créer un [problème GitLab](https://gitlab.com/rubenwardy/minetest_modding_book/-/issues).
* Postez-le dans le [sujet du forum](https://forum.minetest.net/viewtopic.php?f=14&t=10729).
* [Contactez-moi](https://rubenwardy.com/contact/).
* Envie de contribuer ?
  [Lisez le README](https://gitlab.com/rubenwardy/minetest_modding_book/-/blob/master/README.md).
